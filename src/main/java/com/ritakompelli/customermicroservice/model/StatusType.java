package com.ritakompelli.customermicroservice.model;

public enum StatusType {
	NEW,
	INPROCESS,
	RESOLVED,
	CLOSED
}
