package com.ritakompelli.customermicroservice.model;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.validation.annotation.Validated;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;


@Entity
@Table(name = "customer")
@Validated
@Builder
public class Customer {



	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cust_id")
 	private Long custId;
    
    @JsonProperty(value = "custName", required = true)
    @NotEmpty(message = "customer name cannot be empty")
	private String custName;
    
    @JsonProperty(value = "email", required = true)
    @NotEmpty(message = "customer name cannot be empty")
	private String email;
	
	@JsonProperty(value = "phoneNumber", required = true)
	private String phoneNumber;
	
	@JsonProperty(value = "statusType", required = true)
	private StatusType statusType;

	public Customer() {
		super();
	}

	public Customer(Long custId,String custName,String email, String phoneNumber,StatusType statusType) {
		
		this.custId = custId;
		this.custName = custName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.statusType = statusType;
	}
	 
    public Long getCustId() {
		return custId;
	}


	public void setCustId(Long custId) {
		this.custId = custId;
	}


	public String getCustName() {
		return custName;
	}


	public void setCustName(String custName) {
		this.custName = custName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public StatusType getStatusType() {
		return statusType;
	}


	public void setStatusType(StatusType statusType) {
		this.statusType = statusType;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		return Objects.equals(custId, other.custId) && Objects.equals(custName, other.custName)
				&& Objects.equals(email, other.email) && Objects.equals(phoneNumber, other.phoneNumber)
				&& statusType == other.statusType;
	}


	@Override
	public int hashCode() {
		return Objects.hash(custId, custName, email, phoneNumber, statusType);
	}


	@Override
	public String toString() {
		return "Customer {custId=" + custId + ", custName=" + custName + ", email=" + email + ", phoneNumber="
				+ phoneNumber + ", statusType=" + statusType + "}";
	}



	
}

