package com.ritakompelli.customermicroservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "ticket")
@Data
@EqualsAndHashCode(exclude = "customer")
@ToString(exclude = "customer")
@Validated
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Ticket {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@JsonProperty(value = "ticketId", required = true)
    @Column(name = "ticket_id" , updatable = false, nullable = false)
 	private Long ticketId;

	@JsonProperty(value = "custId", required = true)
    @Column(name = "c_id")
 	private Long cId;

    @JsonProperty(value = "message", required = true)
    @NotEmpty(message = "ticket message cannot be empty")
    private String message;
    
	@JsonProperty(value = "statusType", required = true)
	private StatusType statusType;

    @JsonProperty(value = "email", required = true)
    @NotEmpty(message = "customer name cannot be empty")
	private String email;
	
	@JsonProperty(value = "phoneNumber", required = true)
	private String phoneNumber;

    
}
