package com.ritakompelli.customermicroservice.controller;

import static org.springframework.http.HttpStatus.CREATED;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ritakompelli.customermicroservice.model.Customer;
import com.ritakompelli.customermicroservice.model.Ticket;
import com.ritakompelli.customermicroservice.service.CustomerService;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("api/cust-svc")
@CrossOrigin(origins = "*")
@Slf4j
@OpenAPIDefinition(
        info = @Info(title = "Customer API", contact = @Contact(name = "dev", email = "developer@gmail.com")),
        servers = {@Server(url = "http://localhost:8222"), @Server(url = "http://localhost:8333")}
)
public class CustomerController {
	
	private final CustomerService customerService;
	
	public CustomerController(CustomerService customerService) {
		log.info("C-Contructor....");
		this.customerService = customerService;
	}
	
	/**
	 * Saves a Customer entity.
	 * @param entity must not be {@literal null}.
	 * @return the saved entity; will never be {@literal null}.
	 */	
	@PostMapping
    @ResponseStatus(CREATED)
    @Operation(method = "save customer", responses = {
            @ApiResponse(responseCode = "201", description = "saving the order"),
            @ApiResponse(responseCode = "400", description = "invalid order schema passed"),
            @ApiResponse(responseCode = "404", description = "invalid url passed"),
    })
	public Customer saveCustomer(@RequestBody Customer customer) {
		return this.customerService.saveCustomer(customer);
	}
	
	/**
	 * Retrieves Customer entity by its id.
	 *
	 * @param id must not be .
	 * @return the entity with the given id 
	 * @throws IllegalArgumentException
	 */
	@GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	@Operation(method="fetch customer by id",responses= {
			@ApiResponse(responseCode ="201",description = "fetch customer by ID"),
			@ApiResponse(responseCode ="404",description = "invalid url passed")
	})
	public Customer fetchCustomerById(@PathVariable("id") long custId) {
		//Customer custObj = this.customerService.fetchCustomerById(custId);
		return this.customerService.fetchCustomerById(custId);
		
	}
	

	@PostMapping(value="/ticket/customer/{custId}")
    @ResponseStatus(CREATED)
    @Operation(method = "save a ticket", responses = {
            @ApiResponse(responseCode = "201", description = "saving the ticket"),
            @ApiResponse(responseCode = "400", description = "invalid ticket schema passed"),
            @ApiResponse(responseCode = "404", description = "invalid url passed"),
    })

	public Ticket saveTicket(@PathVariable("custId") long custId,@RequestBody Ticket ticket) {
		ticket.setCId(custId);
		return this.customerService.saveTicket(ticket);
	}
	
	
	@GetMapping(value = "/customer/{custId}/ticket/{ticketId}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	@Operation(method="fetch message by customer ID and ticket ID",responses= {
			@ApiResponse(responseCode ="201",description = "fetching message by Customer Id and Ticket Id"),
			@ApiResponse(responseCode ="404",description = "invalid url passed"),
			@ApiResponse(responseCode = "400", description = "invalid ticket schema passed")
	})
	public Ticket fetchBycustomerIdticketId(@PathVariable("custId") long custId,@PathVariable("ticketId") long ticketId) {
		return this.customerService.fetchBycustomerIdticketId(custId,ticketId);
		
	}
	
	@GetMapping(value = "/customer/{custId}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	@Operation(method="fetch all tickets by customer id",responses= {
			@ApiResponse(responseCode ="201",description = "fetching tickets by customer ID"),
			@ApiResponse(responseCode ="404",description = "invalid url passed")
	})
	public Set<Ticket> fetchTicketsBycustomerId(@PathVariable("custId") long custId) {
		//Customer custObj = this.customerService.fetchCustomerById(custId);
		return this.customerService.fetchTicketsBycustomerId(custId);
		
	}	
	
	@GetMapping(value = "/tickets", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	@Operation(method="fetch all tickets data",responses= {
			@ApiResponse(responseCode ="201",description = "fetching all the tickets data"),
			@ApiResponse(responseCode ="404",description = "invalid url passed")
	})
	public  List<Ticket> fetchAlltickets() {
		return this.customerService.fetchAlltickets();
		
	}

}
