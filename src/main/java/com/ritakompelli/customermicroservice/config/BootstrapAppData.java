package com.ritakompelli.customermicroservice.config;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.ritakompelli.customermicroservice.model.Customer;
import com.ritakompelli.customermicroservice.model.StatusType;
import com.ritakompelli.customermicroservice.repository.CustomerRepository;

import lombok.RequiredArgsConstructor;


@Component
@RequiredArgsConstructor
public class BootstrapAppData  implements ApplicationListener<ApplicationReadyEvent> {

	private final CustomerRepository customerRepository;
	private final Faker faker = new Faker();
	 
	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		// TODO Auto-generated method stub
		Customer customer = Customer
                .builder()
                .custName(faker.name().fullName())
                .email(faker.name().firstName() + "@gmail.com")
                .phoneNumber("+918888888888")
                .statusType(StatusType.NEW)
                .build();
		this.customerRepository.save(customer);
	}
}
