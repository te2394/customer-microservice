package com.ritakompelli.customermicroservice.config;

import com.ritakompelli.customermicroservice.repository.TicketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class DBHealthIndicator implements HealthIndicator {

    private final TicketRepository ticketRepository;

    @Override
    public Health health() {
        return ticketRepository.count() >= 0 ?
                Health.up().withDetail("DB-status", "UP").build() : Health.down().withDetail("DB-status", "DOWN").build();
    }
}

@Component
@RequiredArgsConstructor
class KafkaHealthIndicator implements HealthIndicator {
    @Override
    public Health health() {
        return Health.up().withDetail("Kafka-status", "UP").build();
    }
}