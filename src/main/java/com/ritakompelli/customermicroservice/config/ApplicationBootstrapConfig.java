package com.ritakompelli.customermicroservice.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ApplicationBootstrapConfig implements CommandLineRunner {
	@Override
    public void run(String... args) throws Exception {
        System.out.println(" Hello world from Spring Boot application :::: ");
    }
}
