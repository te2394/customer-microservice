package com.ritakompelli.customermicroservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ritakompelli.customermicroservice.model.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer,Long>{

}
