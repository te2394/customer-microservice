package com.ritakompelli.customermicroservice.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ritakompelli.customermicroservice.model.Ticket;

@Repository
public interface TicketRepository extends JpaRepository<Ticket,Long>{
	
    @Query("select t from Ticket t where t.ticketId = :ticketId and t.cId = :custId")
    Ticket fetchBycustomerIdticketId(Long custId,Long ticketId);
    
    @Query("select t from Ticket t where t.cId = :custId")
    Set<Ticket> fetchTicketsBycustomerId(Long custId);
    

}
