package com.ritakompelli.customermicroservice.service;

//import org.springframework.cloud.stream.messaging.Source;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
//import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import com.ritakompelli.customermicroservice.event.EventType;
import com.ritakompelli.customermicroservice.event.TicketEvent;
import com.ritakompelli.customermicroservice.model.Customer;
import com.ritakompelli.customermicroservice.model.Ticket;
import com.ritakompelli.customermicroservice.repository.CustomerRepository;
import com.ritakompelli.customermicroservice.repository.TicketRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerService {
	private final CustomerRepository customerRepository;
	private final TicketRepository ticketRepository;
	//private final Source source;

	
	
	/*
	 * public CustomerService(CustomerRepository customerRepository,TicketRepository
	 * ticketRepository,Source source) { super(); this.customerRepository =
	 * customerRepository; this.ticketRepository = ticketRepository; this.source =
	 * source; }
	 */
	 
 	public Customer saveCustomer(Customer customer) {
 		return this.customerRepository.save(customer);
 		
 	}
 	public Customer fetchCustomerById(long custId) {
 		return this.customerRepository.findById(custId).orElseThrow(()->new IllegalArgumentException("Invalid Argument passed"));
 		
 	}
 	 @CircuitBreaker(name="inventoryservice", fallbackMethod = "fallback")
 	public Ticket saveTicket(Ticket ticket) {
 		Ticket tket = this.ticketRepository.save(ticket);
 		//final TicketEvent tickteEvent = new TicketEvent(ticket,EventType.TICKET_PENDING,LocalDateTime.now());
 		//this.source.output().send(MessageBuilder.withPayload(tickteEvent).build());
 		return tket;
 		
 		
 	}
     private Ticket fallback(Exception exception){
         log.error("Exception while connection to inventory microservice :: {}", exception.getMessage());
         return Ticket.builder().ticketId(1111L).build();
     } 	 
 	public Ticket fetchBycustomerIdticketId(long custId,long ticketId) {
 		return this.ticketRepository.fetchBycustomerIdticketId(custId,ticketId);
 		
 	}
 	public Set<Ticket> fetchTicketsBycustomerId(long custId) {
 		return this.ticketRepository.fetchTicketsBycustomerId(custId);
 		
 	}
 	public List<Ticket> fetchAlltickets() {
 		return this.ticketRepository.findAll();
 		
 	}

}
