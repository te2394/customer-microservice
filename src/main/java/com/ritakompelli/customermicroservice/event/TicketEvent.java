package com.ritakompelli.customermicroservice.event;

import java.time.LocalDateTime;

import com.ritakompelli.customermicroservice.model.Ticket;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class TicketEvent {
    private Ticket ticket;

    private EventType eventType;

    private LocalDateTime timestamp;
}
